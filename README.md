# Assignment 4 - Dynamic  Webpage

## Goal of this assignment

Goal of this assignmet is to build a dynamic webpage using “vanilla” JavaScript (no frameworks).

## Specs of the assignment 

1. The Bank – an area where you will store funds
2. Work – an area to increase your earnings and deposit cash into your bank balance
3. Laptops – an area to select and display information about the merchandise

More detailed specs you can find in the assignment [pdf](Assignment_4_JS_Komputer_Store_App.pdf)

## Hosted webpage

You can find web page hosted in GitLab [pages](https://segdev.gitlab.io/assignment-4-dynamic-webpage/)
