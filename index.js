const bankBalanceElement = document.getElementById("bank-balance")
const loanElement = document.getElementById("loan")
const loanBalanceElement = document.getElementById("loan-balance")
const loanButtonElement = document.getElementById("loan-button")
const workBalanceElement = document.getElementById("work-balance")
const toBankButtonElement = document.getElementById("to-bank-button")
const workButtonElement = document.getElementById("work-button")
const repayButtonElement = document.getElementById("repay-button")
const selectLaptopElement = document.getElementById("select-laptop")
const laptopsFeaturesElement = document.getElementById("laptops-features")
const imageSectionElement = document.getElementById("image-section")
const laptopInfoSectionElement = document.getElementById("laptop-info-section")
const buySectionElement = document.getElementById("buy-section")
const priceElement = document.getElementById("price")
const buyButtonElement = document.getElementById("buy-button")

// Array of computer data
let laptops = []
let workBalance = 0
let bankBalance = 0
let loanBalance = 0
// Currently selected computer
let selectedLaptop 


// Fetching computer data from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToSelect(laptops))


// Function gets API data and calls for every object function that appends
// it to the laptops array. Also assigns price of first element and calls
// fuction that displays starting information when you open page
const addLaptopsToSelect = laptops => {
    laptops.forEach(laptop => addLaptopToSelect(laptop))
    priceElement.innerText = laptops[0].price
    addStartingInfoAndFeatures(laptops[0])
}


// Function that upper function calls for every object. Function creates option
// element and appends it to the DOM
const addLaptopToSelect = laptop => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    selectLaptopElement.appendChild(laptopElement)
}


// Function add starting information when page is loaded for the first time
const addStartingInfoAndFeatures = laptop => {
    // Displays all specs of the computer
    laptop.specs.forEach(spec => displayFeatures(spec))
    // Calls function that displays image, name, title and description
    displayInformation(laptop)
    // Assigns current laptop
    selectedLaptop = laptop
    // Updates work-, bank- and loan balance to the DOM
    workBalanceElement.innerText = workBalance
    bankBalanceElement.innerText = bankBalance
    loanBalanceElement.innerText = loanBalance
}


// Function is called when user changes computer selection
const handleLaptopSelectChange = e => {
    // Assing choisen computer
    selectedLaptop = laptops[e.target.selectedIndex]
    // Assign its current price
    priceElement.innerText = selectedLaptop.price
    // Remove previous computers information from DOM
    elementToRemoveWhenSelectChange()
    // Dispays current computers specs. This foreach calls for helper funcion
    // to display all new specs under selection
    selectedLaptop.specs.forEach(spec => displayFeatures(spec))
    // This helper function displays newly chosen computers image,
    //title and description 
    displayInformation(selectedLaptop)
}


// Helper function to remove previous computers information so new info
// can be appended to the DOM. Array holds all elements that is wanted
// to be removed for easier expendability.
const elementToRemoveWhenSelectChange = () => {
    const elements = [laptopsFeaturesElement, imageSectionElement, laptopInfoSectionElement]
    for(let element of elements){
        // For each element in array calls helpers function
        removeElement(element)
    }
}


// Removes all elements inside given element
const removeElement = elementToRemove => {
    while(elementToRemove.firstChild){
        elementToRemove.removeChild(elementToRemove.lastChild)
    }
}


// Helper function to display selected computers specs under selection
const displayFeatures = spec => {
    const specElement = document.createElement("p")
    specElement.appendChild(document.createTextNode(spec))
    laptopsFeaturesElement.appendChild(specElement)
}


// Helper function to dispaly selected computers image, title and description
async function displayInformation(laptop) {
    // Fetches computers picture from API and creates URL object
    const response = await fetch(`https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`)
    const blob = await response.blob()
    const url = URL.createObjectURL(blob)
    // Creates new image element with size of 150 x 150 and appends it to the DOM
    const imageElement = document.createElement("img")
    imageElement.src = url
    imageElement.alt = `Laptop model ${laptop.title}`
    imageElement.width = "150"
    imageElement.height = "150"
    imageSectionElement.appendChild(imageElement)
    // Creates header element of computers title and appends it to the DOM
    const titleElement = document.createElement("h2")
    titleElement.innerText = laptop.title
    laptopInfoSectionElement.appendChild(titleElement)
    //  Creates paragraph element of computers description and appends
    // it to the DOM
    const descriptionElement = document.createElement("p")
    descriptionElement.innerText = laptop.description
    laptopInfoSectionElement.appendChild(descriptionElement)
}
/*
const displayInformation = laptop => {
    const imageElement = document.createElement("img")
    imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`
    imageElement.alt = `Laptop model ${laptop.title}`
    imageElement.width = "300"
    imageElement.height = "300"
    imageSectionElement.appendChild(imageElement)

    const titleElement = document.createElement("h2")
    titleElement.innerText = laptop.title
    laptopInfoSectionElement.appendChild(titleElement)

    const descriptionElement = document.createElement("p")
    descriptionElement.innerText = laptop.description
    laptopInfoSectionElement.appendChild(descriptionElement)
}*/


// Function is called every time user clicks work button. Every click increases
// work balanse and new balance is dispalayed
const handleWork = () => {
    workBalance += 100
    workBalanceElement.innerText = workBalance
}


// Function is called every time user clicks bank button that transfers pay to
// bank account. Function check first if user have loan and then calls for right
// helper function
const handleBank = () => {
    if(workBalance){
        loanBalance ? bankTransferWhenLoanTrue() : bankTransferWhenLoanFalse()
    }
}


// Helper function if user doesn't have a loan. Function tranfers whole pay 
// to bank account
const bankTransferWhenLoanFalse = () => {
    bankBalance += workBalance
    workBalance = 0
    workBalanceElement.innerText = 0
    bankBalanceElement.innerText = bankBalance
}


// Helper function if user have a loan. Function tranfers 10% of pay towards
// payin loan and rest to the bank account
const bankTransferWhenLoanTrue = () => {
    // First function checks if 10% of pay is bigger or equal than loan
    if(workBalance*0.1 >= loanBalance){
        // If 10% is bigger than whole loan, function decreases work balance
        // and pays loan. After that updates new balances to the DOM
        workBalance -= loanBalance, bankBalance += workBalance
        loanBalance = 0, workBalance = 0
        loanBalanceElement.innerText = loanBalance
        bankBalanceElement.innerText = bankBalance
        workBalanceElement.innerText = workBalance
        // Hides again loan line in bank and repay all button in work 
        loanElement.style.display = "none"
        repayButtonElement.style.display = "none"
    }
    else {
        // Function decreases loan by 10% of pay and adds rest of pay to bank 
        loanBalance -= workBalance*0.1
        bankBalance += workBalance*0.9
        workBalance = 0
        // Updates then new balances to the DOM
        loanBalanceElement.innerText = loanBalance
        bankBalanceElement.innerText = bankBalance
        workBalanceElement.innerText = workBalance
    }
}


// Function is calles when user tries to take new loan.
const handleLoan = () => {
    // If user don't have any savings in bank alert is called and loan is not given
    if(!bankBalance) {
        alert("Sorry you need some bank balance first!")
    }
    // If user have already unpaid loan alert is called and loan is not given
    else if (loanBalance){
        alert("Sorry but you need to pay first your current loan!")
    }
    else {
        // Promts loan amount from user
        const loanAmount = prompt(`With your balance you can take a loan between 0-${bankBalance*2}. \nHow much you would like to loan?`)
        // If amount is negative alert is called
        if (loanAmount < 0){
            alert("Sorry but you can't take negative loan..")
        }
        // If user tries to take too big loan alert is also called
        else if (loanAmount > bankBalance*2) {
            alert("Sorry but you can't get that big loan")
        }
        else {
            // Loan is handled by increasing bank balance by taken loan
            // and adding loan amount to the loan balance
            bankBalance += parseFloat(loanAmount)
            loanBalance = parseFloat(loanAmount)
            // Both balances is updated to the DOM
            loanBalanceElement.innerText = loanBalance
            bankBalanceElement.innerText = bankBalance
            // After loan is taken hidden elements are displayed: loan amount in
            // bank and repay whole loan button in work
            loanElement.style.display = "block"
            repayButtonElement.style.display = "block"
        }
    }
}


// Function is called when user have loan and wants to use all work pay balance
// towards paying the loan
const handleRepayAll = () => {
    // First checks if user have any pay balance, if not alert it called
    if(!workBalance){
        alert("You need to do some work first before you can pay")
    }
    // Then function checks if pay balance is bigger than loan. If pay balance is bigger
    // or equal function pays of the loan and leaves rest of the pay in pay balance
    else if(workBalance >= loanBalance){
        workBalance -= loanBalance
        loanBalance = 0
        // Updates new balances to the DOM
        workBalanceElement.innerText = workBalance
        loanBalanceElement.innerText = loanBalance
        // Hides loan element in bank and repay all button in work
        loanElement.style.display = "none"
        repayButtonElement.style.display = "none"
    }
    else {
        // If pay is smaller than loan, function transfers whole pay balance towards paying loan
        loanBalance -= workBalance
        workBalance = 0
        workBalanceElement.innerText = workBalance
        loanBalanceElement.innerText = loanBalance
    }
}


// Function is called when user tries to buy new computer
const handleBuyNow = () => {
    // First function checks if user have enough money to buy computer, if not alert is called
    if(selectedLaptop.price > bankBalance){
        alert("Sorry not enough money to buy this beast..")
    }
    else{
        // If user have enough money, congratulation alert is called as a sign of successful shop
        alert(`CONGRATULATION!! You just bought yourself a ${selectedLaptop.title}!!`)
        // Laptops price is deducted from users bank balance
        bankBalance -= selectedLaptop.price
        bankBalanceElement.innerText = bankBalance
    }
}

// Event listener for changing comptures in selection list
selectLaptopElement.addEventListener("change", handleLaptopSelectChange)
// Event listener for work button to work
workButtonElement.addEventListener("click", handleWork)
// Event listener for bank button to transfer pay to bank
toBankButtonElement.addEventListener("click", handleBank)
// Event listener for loan button to take a new loan
loanButtonElement.addEventListener("click", handleLoan)
// Event listener for repay all button to pay loan with whole pay
repayButtonElement.addEventListener("click", handleRepayAll)
// Event listener for buy button to buy new computer
buyButtonElement.addEventListener("click", handleBuyNow)